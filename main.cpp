#include "DBHandler.h"

#include <QtCore/QCoreApplication>
#include <QtSql/QtSql>


QSqlDatabase GetDb(QString const& conName, QString const& dbName = ":memory:")
{
    if (QSqlDatabase::contains(conName))
        return QSqlDatabase::database(conName);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", conName);
    db.setDatabaseName(dbName);
    //db.setConnectOptions("QSQLITE_BUSY_TIMEOUT=2"); // any value given makes 0 timeout
    if (not db.open())
    {
        qDebug() << "db could not be opened: " << db.lastError() << ", con. name: "
                 << db.connectionName() << ", db name: " << db.databaseName();
    }
    return db;
}

void CreateTable(QSqlDatabase const& db)
{
    db.exec("CREATE TABLE people (name VARCHAR(32), last_name VARCHAR(32), age INT);");
    if (db.lastError().isValid())
        qDebug() << "error:" << db.lastError();
}

void Query(QString const& queryStr, QSqlDatabase const& db)
{
    QSqlQuery query(queryStr, db);
    if (query.lastError().isValid())
        qDebug() << "query failed: " << queryStr << ", error:" << query.lastError();
}

void PrintTable(QSqlDatabase const& db)
{
    qDebug() << "--- Printing table using connection: " << db.connectionName() << ", db name: " << db.databaseName();
    QSqlQuery query("SELECT name, last_name, age FROM people", db);
    if (query.lastError().isValid())
        qDebug() << "insert query error:" << query.lastError();
    while (query.next())
    {
        qDebug() << query.record();
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QSqlDatabase db1 = GetDb("con1","on_disk.db");
    QSqlDatabase db2 = GetDb("con2","on_disk.db");

    CreateTable(db1);

    Query("INSERT INTO people VALUES ('Michael', 'Jordan', 45);", db1);
    Query("INSERT INTO people VALUES ('Kobe', 'Bryant', 30);", db2);

    PrintTable(db1);    // prints Jordan Bryant
    PrintTable(db2);    // prints Jordan Bryant

    qDebug() << "\n======\n";

    QSqlDatabase db3 = GetDb("con3"); // in memory
    QSqlDatabase db4 = GetDb("con4"); // another in memory

    CreateTable(db3);
    CreateTable(db4);

    Query("INSERT INTO people VALUES ('Michael', 'Jordan', 45);", db3);
    Query("INSERT INTO people VALUES ('Kobe', 'Bryant', 30);", db4);

    PrintTable(db3);    // prints Jordan
    PrintTable(db4);    // prints Bryant

    qDebug() << "\n======\n";

    DBHandler dbh("on_disk.db");

    auto resultFun = [] (QSqlError const& p_error) { qDebug() << "DBHandler client received sql error: " << p_error; };
    // note that order of execution is not defined
    dbh.query("INSERT INTO people VALUES ('baba', 'jaga', 66);", resultFun);
    dbh.query("INSERT INTO people VALUES ('baba2', 'jaga', 66);", resultFun);
    dbh.query("INSERT INTO people VALUES ('baba3', 'jaga', 66);", resultFun);

    auto selectResultFun = [] (QSqlError const& p_error, QList<QSqlRecord> const& p_records)
    {
        qDebug() << "DBHandler received sql select error: " << p_error << ", selected records: " << p_records;
    };
    dbh.selectQuery("SELECT name FROM people where age=?", selectResultFun, QVariant(45));

    return a.exec();
}
