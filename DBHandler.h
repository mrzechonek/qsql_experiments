#ifndef DBHANDLER_H
#define DBHANDLER_H

#include <QtSql/QtSql>
#include <QList>

class DBHandler : QObject
{
    Q_OBJECT
public:
    DBHandler(QString const& p_dbName);

    typedef std::function<void (QSqlError const&)> basic_callback;
    typedef std::function<void (QSqlError const&, QList<QSqlRecord> const&)> select_callback;

    void query(QString const& p_query, basic_callback);

    // todo: the below will require at least one argument of query. add version that will support just p_query
    template <typename... Args_t>
    void selectQuery(QString const& p_query, select_callback p_callback, /*const*/ Args_t&&... p_args)
    {
        QList<QVariant> paramsList;
        fillParams(paramsList, std::forward<Args_t>(p_args)...);

        auto    future          = QtConcurrent::run( runQuery, p_query, m_dbName, p_callback, paramsList );
        auto*   queryWatcher    = new QFutureWatcher< std::tuple<QSqlError, QList<QSqlRecord>, select_callback> >(this);
        connect(queryWatcher, SIGNAL(finished()), SLOT(onSelectQueryFinished()));
        queryWatcher->setFuture(future);
    }

private slots:
    void onQueryFinished();
    void onSelectQueryFinished();

private:
    template <typename... Args_t>
    static void fillParams(QList<QVariant>& p_paramsList, const QVariant&& p_param, /*const*/ Args_t&&... p_args)
    {
        p_paramsList << p_param;
        fillParams(p_paramsList, std::forward<Args_t>(p_args)...);
    }

    static void fillParams(QList<QVariant>& /*p_paramsList*/)
    {
    }

    static std::pair<QSqlError, DBHandler::basic_callback> runQuery(QString const& p_query, QString const& p_dbName,
                                                                    DBHandler::basic_callback p_callback);

    static std::tuple<QSqlError, QList<QSqlRecord>, DBHandler::select_callback> runQuery(QString const& p_query, QString const& p_dbName,
                                                                                         DBHandler::select_callback p_callback,
                                                                                         QList<QVariant> const& p_paramsList);

    QString m_dbName;
};

#endif // DBHANDLER_H
