#include "DBHandler.h"

#include <QtConcurrentRun>
#include <QFutureWatcher>

DBHandler::DBHandler(QString const& p_dbName) : m_dbName(p_dbName)
{
}

QString CreateDbConnectionName(QString const&  p_dbName)
{
    qDebug() << "thread name: " << QThread::currentThreadId();

    // todo: QThread::currentThreadId() should rather not be used. What instead?
    return p_dbName + QString::number(QThread::currentThreadId());
}

QSqlDatabase CreateDbConnection(QString const&  p_conName, QString const& p_dbName)
{
    qDebug() << " creating connection " << p_conName << " to " << p_dbName;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", p_conName);  // hardcoded SQL driver
    db.setDatabaseName(p_dbName);
    //db.setConnectOptions("QSQLITE_BUSY_TIMEOUT=2"); // any value given makes 0 timeout
    if (not db.open())
    {
        qWarning() << "db could not be opened: " << db.lastError() << ", con. name: "
                 << db.connectionName() << ", db name: " << db.databaseName();
    }
    return db;
}

std::pair<QSqlError, DBHandler::basic_callback> DBHandler::runQuery(QString const& p_query, QString const& p_dbName,
                                                                    DBHandler::basic_callback p_callback)
{
    qDebug() << "___starting query: " << p_query;
    QString conName = CreateDbConnectionName(p_dbName);
    qDebug() << "dbname: " << p_dbName << ", con name: " << conName;
    QSqlError error;
    {
        QSqlDatabase db = CreateDbConnection(conName, p_dbName);
        QSqlQuery q(p_query, db);
        error = q.lastError();
        //q.finish();  // it is probably not needed
        //db.close();
    }
    QSqlDatabase::removeDatabase(conName);

    qDebug() << "___finishing query: " << p_query;

    return std::make_pair(error, p_callback);
}

std::tuple<QSqlError, QList<QSqlRecord>, DBHandler::select_callback> DBHandler::runQuery(QString const& p_query, QString const& p_dbName,
                                                                                         DBHandler::select_callback p_callback,
                                                                                         QList<QVariant> const& p_paramsList)
{
    qDebug() << "___starting select query: " << p_query;
    QString conName = CreateDbConnectionName(p_dbName);
    qDebug() << "dbname: " << p_dbName << ", con name: " << conName;
    QSqlError error;
    QList<QSqlRecord> results;
    {
        QSqlDatabase db = CreateDbConnection(conName, p_dbName);

        QSqlQuery q(db);
        q.prepare(p_query); // shall we verify return value at this stage?
        for(auto& param : p_paramsList) { q.addBindValue(param); }
        q.exec();

        error = q.lastError();
        if (not error.isValid())
        {
            while (q.next())
                results << q.record();
        }
        //q.finish();  // it is probably not needed
        //db.close();
    }
    QSqlDatabase::removeDatabase(conName);

    qDebug() << "___finishing query: " << p_query;

    return std::make_tuple(error, results, p_callback);
}

void DBHandler::query(QString const& p_query, basic_callback p_callback)
{
    auto    future          = QtConcurrent::run( runQuery, p_query, m_dbName, p_callback );
    auto*   queryWatcher    = new QFutureWatcher< std::pair<QSqlError, basic_callback> >(this);
    connect(queryWatcher, SIGNAL(finished()), SLOT(onQueryFinished()));
    queryWatcher->setFuture(future);
}

void fillParams(QList<QVariant>& /*p_paramsList*/)
{
}

template <typename... Args_t>
void fillParams(QList<QVariant>& p_paramsList, const QVariant&& p_param, /*const*/ Args_t&&... p_args)
{
    p_paramsList << p_param;
    fillParams(p_paramsList, std::forward<Args_t>(p_args)...);
}

void DBHandler::onQueryFinished()
{
    auto* queryWatcher = dynamic_cast<QFutureWatcher<std::pair<QSqlError, DBHandler::basic_callback> >*> (QObject::sender());
    if (queryWatcher)
    {
        auto result = queryWatcher->result();
        qDebug() << "error code of query is " << result.first;
        basic_callback& callback = result.second;
        callback( result.first );

        queryWatcher->deleteLater();
    }
    else
        qWarning() << "pointer to query watcher could not be obtained in query results finished";
}

void DBHandler::onSelectQueryFinished()
{
    auto* queryWatcher = dynamic_cast<QFutureWatcher<std::tuple<QSqlError,
                                                                QList<QSqlRecord>,
                                                                DBHandler::select_callback>
                                                    >*> (QObject::sender());
    if (queryWatcher)
    {
        auto result = queryWatcher->result();

        auto& sqlError      = std::get<0> (result);
        auto& resultsList   = std::get<1> (result);
        auto& callback      = std::get<2> (result);
        qDebug() << "error code of select query is " << sqlError;
        callback( sqlError, resultsList );

        queryWatcher->deleteLater();
    }
    else
        qWarning() << "pointer to query watcher could not be obtained in query results finished";
}
